<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SiteController::class, 'home'])->name('home');

Route::get('about_us', [SiteController::class, 'aboutUs'])->name('about_us');

Route::get('quotes', [SiteController::class, 'quotes'])->name('quotes');




Route::prefix('user')->group(function() {
    
    Route::get('register', [UserController::class, 'register'])->name('userRegister');

    Route::post('create', [UserController::class, 'store'])->name('userStore');

    Route::get('login', [AuthController::class, 'form'])->name('userLoginForm');

    Route::post('login', [AuthController::class, 'login'])->name('userLogin');

    Route::get('logout', [AuthController::class, 'logout'])->name('userLogout');
    
    Route::get('{id}', [SiteController::class, 'userShow'])->name('userShow');
});

Route::prefix('publications')->group(function() {
    
    Route::get('/', [PublicationController::class, 'index'])->name('publications');

    Route::get('{id}', [PublicationController::class, 'show'])->WhereNumber('id')->name('publicationShow');

    Route::get('create', [PublicationController::class, 'create'])->middleware('auth')->name('publicationCreate');

    Route::post('create', [PublicationController::class, 'store'])->middleware('auth')->name('publicationStore');

    Route::get('{id}/edit', [PublicationController::class, 'edit'])->name('publicationEdit');

    Route::put('{publication}', [PublicationController::class, 'update'])->name('publicationUpdate');

    Route::delete('{publication}', [PublicationController::class, 'delete'])->name('publicationDelete');
});

Route::prefix('comments')->group(function() {

    Route::post('create', [CommentController::class, 'store'])->name('commentStore');

    Route::delete('{comment}', [CommentController::class, 'destroy'])->name('commentDelete');

});

Route::get('admin', function () {
    if (Gate::denies('admin-access')) {
        abort(403);
    }

    echo 'Panel administratora';
})->name('admin-panel');