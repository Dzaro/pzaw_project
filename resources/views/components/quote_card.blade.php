@props(['quote'])


<figure
    class="w-60 rounded-xl bg-transparent border-solid border-2 aboutUsDiv darkAboutUsDiv backdrop-blur publicationsScale hover:shadow-2xl">
    <div class="bg-transparent">
        <img class="w-60" src="{{ $quote['image'] }}" alt="author">
    </div>
    <div class="bg-transparent h-64 quote_card_gradient darkPublicationsArticleBackground">
        <blockquote class="italic text-2xl pt-10 pr-10 pl-10">{{ $quote['quote'] }}</blockquote>
        <figcaption>
            <p></p>
            <p class="text-right text-xl pr-10 pl-10 darkpurple dark:purple">{{ $quote['hero'] }}</p>
            <p class="text-right pr-10 pl-10 darkpurple dark:purple">{{ $quote['role'] }}</p>
        </figcaption>
    </div>
</figure>