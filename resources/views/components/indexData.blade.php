@props(['publicationData'])

<div class="pb-20">
    <!-- ADDITIONAL STYLE TO DIV BELOW publicationsDivBackground border-solid border-2 aboutUsDiv -->
    <div class="h-fit justify-center flex flex-wrap gap-20 mt-20 mx-20">
        @foreach($publicationData as $publication)
            <div class="flex justify-between publicationsArticle border-solid border-2 p-10 publicationsScale aboutUsDiv darkPublicationsArticleBackground dark:darkAboutUsDiv">
                <div>
                    <h1 class="text-3xl">{{ $publication["title"] }}</h1>

                    @if($publication['author']['deleted_at'] != "")
                        <p class="text-xl text-gray-600 dark:text-gray-400"> <s>{{ $publication['author']['name'] }}</s>, {{ $publication['created_at']->diffForHumans() }}</p>
                    @else
                        <p class="text-xl text-gray-600 dark:text-gray-400"><a href="{{ route('userShow', ['id' => $publication->author_id]) }}"> {{ $publication['author']['name'] }}</a>, {{ $publication['created_at']->diffForHumans() }}</p>
                    @endif

                    <p class="mt-5">{{ $publication->excerpt }} <a class="underline purple" href="{{ route('publicationShow', ['id' => $publication->id]) }}">Czytaj więcej</a></p>
                </div>
                <div class="ml-[40px]">
                    @auth
                        @if(auth()->user()->id == $publication->author_id)
                            <a href="{{ route('publicationEdit', ['id' => $publication->id]) }}"><i class="bi bi-pencil text-xl"></i></a>
                        @endif
                    @endauth
                </div>
            </div>
        @endforeach
    </div>
</div>