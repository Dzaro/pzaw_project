@if (session()->has('success'))
<div class="bg-green-400/50 rounded-lg p-5 mt-20 publicationsShowArticle" role="alert">
    <div>
        {{ session('success') }}
    </div>
</div>
@endif

@if (session()->has('warning'))
<div class="flex bg-yellow-100 rounded-lg p-4 mb-4 text-sm text-yellow-700" role="alert">
    <div>
        {{ session('warning') }}
    </div>
</div>
@endif

@if (session()->has('error'))
<div class="flex bg-red-400/50 rounded-lg p-5 mt-20 publicationsShowArticle text-red-700" role="alert">
    <div>
        {{ session('error') }}
    </div>
</div>
@endif