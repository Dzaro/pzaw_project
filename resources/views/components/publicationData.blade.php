@props(['comments'])

<script>
    var list = [];
</script>

<div>
    @foreach ($publicationData as $publication)
        <div class="flex border-solid border-2 rounded-lg aboutUsDiv dark:darkAboutUsDiv py-10 pr-10 mt-20 publicationsShowArticle">
            <div class="w-10">
                @auth
                    @if(auth()->user()->id == $publication->author_id)
                        <div class="w-10 flex justify-center"><a href="{{ route('publicationEdit', ['id' => $publication->id]) }}"><i class="bi bi-pencil text-4xl"></i></a></div>
                        <div class="w-10 flex justify-center">
                            <form action="{{ route('publicationDelete', ['publication' => $publication]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit"><i class="bi bi-trash text-4xl"></i></button>
                            </form>
                        </div>
                    @endif
                @endauth
            </div>
            <div>
                <h1 class="text-5xl">{{ $publication["title"] }}</h1>
                @if($publication['author']['deleted_at'] != "")
                    <p class="mt-2 text-xl text-gray-600 dark:text-gray-400"><s>{{ $publication['author']['name'] }}</s>, {{ $publication['created_at']->diffForHumans() }}</p>
                @else()
                    <p class="mt-2 text-xl text-gray-600 dark:text-gray-400"><a href="{{ route('userShow', ['id' => $publication->author_id]) }}">{{ $publication['author']['name'] }}</a>, {{ $publication['created_at']->diffForHumans() }}</p>
                @endif
                <p class="mt-5 text-lg">{{ $publication["content"] }}</p>
            </div>
        </div>
        <div id="commentsButton" class="flex justify-between darkPublicationsArticleBackground border-solid border-2 rounded-lg aboutUsDiv dark:darkAboutUsDiv px-10 py-5 mt-10 publicationsShowArticle hover:cursor-pointer">
            <div class="flex items-center">
                <p class="text-xl">Komentarze</p>
            </div>
            <div class="flex items-center">
                <p id="arrow" class="text-3xl duration-300">
                    <i class="bi bi-caret-up"></i>
                </p>
            </div>
        </div>
        <div id="commentsDiv" class="darkPublicationsArticleBackground border-solid border-2 rounded-lg aboutUsDiv dark:darkAboutUsDiv px-10 py-5 publicationsShowArticle hidden translate-y-Negative2px">
            <div>
                @php
                    $action = route('commentStore');
                    $comment = null;
                @endphp

                @auth
                <form action="{{$action}}" method="POST">
                    <div class="flex flex-col items-start">
                        @csrf
                        <input type="hidden" name="author_id" value="{{auth()->user()->id}}">
                        <input type="hidden" name="publication_id" value="{{ $publication['id'] }}">
                        <input type="hidden" name="parent_id" value="">
                        <input type="text" name="content" class="apperance-none text-xl bg-transparent border-none w-[500px] outline-none focus:placeholder:purple focus:purple focus:[border-color:rgb(152,106,220)] [border-bottom:solid] [border-color:black] [border-width:2px] dark:[border-color:white] dark:focus:[border-color:rgb(152,106,220)]" placeholder="Dodaj komentarz">
                        <div class="flex">
                            @error('content')
                                <p class="text-red-500 text-xl italic mr-5">{{ $message }}</p>
                            @enderror
                            @error('author_id')
                                <p class="text-red-500 text-xl italic mr-5">{{ $message }}</p>
                            @enderror
                            @error('publication_id')
                                <p class="text-red-500 text-xl italic mr-5">{{ $message }}</p>
                            @enderror
                            @error('parent_id')
                                <p class="text-red-500 text-xl italic mr-5">{{ $message }}</p>
                            @enderror
                            <input type="submit" class="purple hover:cursor-pointer hover:underline text-xl" value="Dodaj">
                        </div>
                    </div>
                </form>
                @else
                <p class="mt-2 text-xl text-gray-600 dark:text-gray-400 italic">Zaloguj się, aby dodać komentarz</p>
                @endauth
            </div>
            @foreach ($comments as $comment)

                @if($comment['parent_id'] == "")
                    <div class="ml-0 my-5 comment" id="comment{{$comment['id']}}">

                        @if($comment['deleted_at'] != "")
                        <p class="mt-2 text-xl text-gray-600 dark:text-gray-400"><i>Komentarz został usunięty.</i></p>
                        @else

                            <div class="flex">
                                @if($comment['author']['deleted_at'] != "")
                                    <p class="mt-2 text-xl text-gray-600 dark:text-gray-400"><s>{{ $comment['author']['name'] }}</s>, {{ $comment['created_at']->diffForHumans() }}</p>
                                @else
                                    <p class="mt-2 text-xl text-gray-600 dark:text-gray-400">
                                        <a href="{{ route('userShow', ['id' => $comment->author_id]) }}">{{ $comment['author']['name'] }}</a>
                                        , {{ $comment['created_at']->diffForHumans() }},
                                    </p>
                                @endif

                                @auth
                                <p onclick="createCommentForm({{$comment['id']}})" class="ml-1 mt-2 text-xl text-gray-600 dark:text-gray-400 cursor-pointer italic hover:underline select-none">Odpowiedz</p>
                                @endauth
                                <form action="{{ route('commentDelete', ['comment' => $comment]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Usuń" class="ml-3 mt-2 text-xl text-gray-600 dark:text-gray-400 cursor-pointer italic hover:underline select-none">
                                </form>
                            </div>

                        <p class="text-lg">{{ $comment["content"] }}</p>
                        @endif
                    </div>

                @else   
                    <script>
                        list.push([
                            {{ $comment['id'] }},
                            "{{ $comment['content'] }}",
                            "{{ $comment['author']['name'] }}",
                            "{{ route('userShow', ['id' => $comment->author_id]) }}",
                            {{ $comment['parent_id'] }},
                            "{{ $comment['created_at']->diffForHumans() }}",
                            "{{ $comment['deleted_at'] }}",
                            "{{ $comment['author']['deleted_at'] }}",
                            "{{ route('commentDelete', ['comment' => $comment]) }}"
                        ]);
                    </script>
                @endif

            @endforeach

        </div>
    @endforeach
</div>

<script>
    //global variables
    let commentsButton = document.getElementById("commentsButton");
    let commentsDiv = document.getElementById("commentsDiv");
    let arrow = document.getElementById("arrow");
    
    //script for commentsDiv to unfold and fold
    commentsButton.addEventListener("click", function() {
        if(commentsDiv.classList.contains("hidden")) {
            commentsDiv.classList.remove("hidden")
            // commentsDiv.classList.remove("opacity-0")
            // commentsDiv.classList.add("opacity-1")
            commentsDiv.classList.add("block");
            arrow.style.transform = "rotate(180deg)";
        }
        else if(commentsDiv.classList.contains("block")) {
            commentsDiv.classList.remove("block")
            commentsDiv.classList.add("hidden");
            arrow.style.transform = "rotate(0deg)";
        }
    });

    //script for creating form to add comment
    function createCommentForm(parent_id) {

        let commentForms = document.querySelectorAll('#commentForm');
        commentForms.forEach((element)  => {
            element.remove();
        })

        let parentComment = document.getElementById("comment" + parent_id);
        
        if(!(Array.from(parentComment.children).some(child => child.tagName === 'FORM'))) { //check if parentComment does already have commentForm (prevent creating multiple commentForms)
            const form = document.createElement('form');
            form.action = "{{ route('commentStore') }}";
            form.method = "POST";
            form.id = "commentForm";
            
            //create input elements
            const csrfInput = document.createElement('input');
            csrfInput.type = "hidden";
            csrfInput.name = "_token";
            csrfInput.value = "{{ csrf_token() }}";
            form.appendChild(csrfInput);
            
            const authorIdInput = document.createElement('input');
            authorIdInput.type = "hidden";
            authorIdInput.name = "author_id";
            @auth
            authorIdInput.value = "{{ auth()->user()->id }}";
            @endauth
            form.appendChild(authorIdInput);
            
            const publicationIdInput = document.createElement('input');
            publicationIdInput.type = "hidden";
            publicationIdInput.name = "publication_id";
            publicationIdInput.value = "{{ $publication['id'] }}";
            form.appendChild(publicationIdInput);

            const parentIdInput = document.createElement('input');
            parentIdInput.type = "hidden";
            parentIdInput.name = "parent_id";
            parentIdInput.value = parent_id;
            form.appendChild(parentIdInput);
            
            const contentInput = document.createElement('input');
            contentInput.type = "text";
            contentInput.name = "content";
            contentInput.className = "apperance-none text-xl bg-transparent border-none w-[500px] outline-none focus:placeholder:purple focus:purple focus:[border-color:rgb(152,106,220)] [border-bottom:solid] [border-color:black] [border-width:2px] dark:[border-color:white] dark:focus:[border-color:rgb(152,106,220)]";
            contentInput.placeholder = "Dodaj komentarz";
            form.appendChild(contentInput);
            
            const errorParagraphs = document.createElement('div');
            errorParagraphs.className = "flex";
            
            //create error paragraphs
            ["content", "author_id", "publication_id", "parent_id"].forEach(fieldName => {
                const errorParagraph = document.createElement('p');
                errorParagraph.className = "text-red-500 text-xl italic mr-5";
                errorParagraph.innerText = "@error('" + fieldName + "') {{ $message }} @enderror";
                errorParagraphs.appendChild(errorParagraph);
            });
        
            form.appendChild(errorParagraphs);
        
            //create submit button
            const submitButton = document.createElement('input');
            submitButton.type = "submit";
            submitButton.className = "purple hover:cursor-pointer hover:underline text-xl";
            submitButton.value = "Dodaj";
            form.appendChild(submitButton);
        
            parentComment.appendChild(form);
        }
    }

    //script for creating child comments

    function createComment(id, content, author_name, author_href, parent_id, created_at, is_comment_deleted, is_author_deleted, deleteRoute) {
            let parent = document.getElementById("comment"+parent_id);

            const div = document.createElement('div');
            div.className = "ml-5 pl-5 py-5 comment  [border-left-width:3px] border-gray-400 border-dashed";
            div.id = "comment"+id;
            div.parent_id = parent_id;

            if(is_comment_deleted == "") {

                const divFlex = document.createElement('div');
                divFlex.className = "flex";

                const authorP = document.createElement('p');
                authorP.className = "mt-2 text-xl text-gray-600 dark:text-gray-400";
                
                const authorA = document.createElement('a');
                if(is_author_deleted == "") {
                    authorA.innerHTML = author_name;
                    authorA.href = author_href;
                }
                else {
                    authorA.innerHTML = "<s>"+author_name+"</s>";
                }

                authorP.appendChild(authorA);
                authorP.innerHTML += ", " + created_at;

                divFlex.appendChild(authorP);

                @auth
                const answerToP = document.createElement('p');
                answerToP.className = "ml-1 mt-2 text-xl text-gray-600 dark:text-gray-400 cursor-pointer italic hover:underline select-none";
                answerToP.addEventListener('click', function() {createCommentForm(id)});
                answerToP.textContent = "Odpowiedz";
                divFlex.appendChild(answerToP);
                @endauth
                
                var form = document.createElement('form');
                form.action = deleteRoute;
                form.method = 'POST';

                var csrfInput = document.createElement('input');
                csrfInput.type = 'hidden';
                csrfInput.name = '_token';
                csrfInput.value = '{{ csrf_token() }}';
                form.appendChild(csrfInput);

                var methodInput = document.createElement('input');
                methodInput.type = 'hidden';
                methodInput.name = '_method';
                methodInput.value = 'DELETE';
                form.appendChild(methodInput);

                var submitButton = document.createElement('input');
                submitButton.type = 'submit';
                submitButton.value = 'Usuń';
                submitButton.classList.add('ml-3', 'mt-2', 'text-xl', 'text-gray-600', 'dark:text-gray-400', 'cursor-pointer', 'italic', 'hover:underline', 'select-none');
                form.appendChild(submitButton);
                divFlex.appendChild(form);

                const contentP = document.createElement('p');
                contentP.className = "text-lg";
                contentP.textContent = content;


                div.appendChild(divFlex);
                div.appendChild(contentP);

            }
            else {
                const deletedP = document.createElement('p');
                deletedP.className = "mt-2 text-xl text-gray-600 dark:text-gray-400";
                deletedP.innerHTML = "<i>Komentarz został usunięty.</i>";
                div.appendChild(deletedP);
            }

            //console.log(div);
            parent.appendChild(div);
    }

    for(let i = 0; i < list.length; ++i) {
        //console.log(list[i][0],list[i][1],list[i][2],list[i][3],list[i][4],list[i][5],list[i][6],list[i][7]);
        createComment(list[i][0],list[i][1],list[i][2],list[i][3],list[i][4],list[i][5],list[i][6],list[i][7],list[i][8]);
    }

</script>