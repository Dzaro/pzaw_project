<!DOCTYPE html>
<html lang="pl" class="dark">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }}</title>

    <script src="https://cdn.tailwindcss.com"></script>
    <script>
    tailwind.config = {
        theme: {
            extend: {
                translate: {
                    'Negative2px': '-2px',
                }
            }
        },
        darkMode: 'class',
    }
    </script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    <style type="text/tailwindcss">
        @layer base {
            body {
                font-family:'Poppins', sans-serif;
            }

            h1 {
                @apply text-4xl uppercase font-bold;
            }

            .backgroundBody1 {
                background: linear-gradient(#ffffff, rgb(235, 187, 255) 60% ,rgb(182, 126, 255));
                min-height:calc(100vh - 2.5rem);
            }

            .darkBackgroundBody1 {
                background: linear-gradient(#000000, rgb(14, 17, 35) 60% ,rgb(16, 16, 48));
                min-height:calc(100vh - 2.5rem);
            }

            .backgroundBody2 {
                background-size:40px 40px;
                background-image: radial-gradient(circle at 20px 20px, rgb(70, 70, 70) 0.05em, transparent 0);
                background-attachment: fixed;
                min-height:calc(100vh - 2.5rem);
            }

            .darkBackgroundBody2 {
                background-size:40px 40px;
                background-image: radial-gradient(circle at 20px 20px, rgb(70, 70, 70) 0.05em, transparent 0);
                background-attachment: fixed;
                min-height:calc(100vh - 2.5rem);
            }

            .purple {
                color:rgb(152, 106, 220);
            }

            .darkpurple {
                color:rgb(105, 39, 204);
            }

            .activeSite {
                text-decoration: underline;
                color: rgb(152, 106, 220);
            }

            .spanGradient {
                background: radial-gradient(circle, rgba(0,0,0,1) 52%, rgba(40,40,40,0) 100%);
            }

            .darkSpanGradient {
                background: radial-gradient(circle, rgba(255,255,255,1) 52%, rgba(40,40,40,0) 100%);
            }

            .spanGradientX {
                display:block;
                width:100vw;
                height:2px;
            }

            .homeArrowAnimation {
                transition-duration:0.5s;
                margin-top:75vh;
            }

            .darkHomeArrowAnimation {
                transition-duration:0.5s;
                margin-top:75vh;
            }

            .homeArrowAnimation:hover {
                color:rgb(152, 106, 220);
                transition-duration:0.5s;
                cursor: pointer;
            }

            .darkHomeArrowAnimation:hover {
                color:rgb(105, 39, 204);
                transition-duration:0.5s;
                cursor: pointer;
            }

            .homeContent {
                background-color: rgb(182, 126, 255);
                -webkit-box-shadow: 0px 1px 93px 63px rgb(182, 126, 255);
                -moz-box-shadow: 0px 1px 93px 63px rgb(182, 126, 255);
                box-shadow: 0px 1px 93px 63px rgb(182, 126, 255);
            }

            .darkHomeContent {
                background-color: rgb(16, 16, 48);
                -webkit-box-shadow: 0px 1px 93px 63px rgb(16, 16, 48);
                -moz-box-shadow: 0px 1px 93px 63px rgb(16, 16, 48);
                box-shadow: 0px 1px 93px 63px rgb(16, 16, 48);
            }

            .aboutUsDiv {
                border-image: 
                linear-gradient(
                    170deg,
                    black,
                    rgb(152, 106, 220),
                    rgba(0, 0, 0, 0)
                ) 2;
            }

            .darkAboutUsDiv {
                border-image: 
                linear-gradient(
                    170deg,
                    white,
                    rgb(152, 106, 220),
                    rgba(0, 0, 0, 0)
                ) 2;
            }

            .darkPublicationsDivBackground {
                background: linear-gradient(140deg, rgba(79,53,111,0.20) 15%, rgba(119,84,164,0.45) 54%, rgba(103,88,255,0.10) 88%);
            }
            
            .publicationsArticle {
                width:40vw;
            }

            .publicationsShowArticle {
                width:86vw;
                margin-left:7vw;
                margin-right:7vw;
            }

            .darkPublicationsArticleBackground {
                background: linear-gradient(140deg, rgba(90,98,240,0.4) 15%, rgba(151,103,222,0.6) 54%, rgba(23,0,255,0.2147233893557423) 88%);
                /* backdrop-filter:blur(20px) */
            }
            .publicationsScale {
                transition-duration:0.5s;
            }
            .publicationsScale:hover {
                transform:scale(1.1);
                transition-duration:0.5s;
            }

            .quoteCardDiv {
                width:90vw;
            }

            /* The switch - the box around the slider */
            .switch {
                font-size: 17px;
                position: relative;
                display: inline-block;
                width: 3.5em;
                height: 2em;
            }

            /* Hide default HTML checkbox */
            .switch input {
                opacity: 0;
                width: 0;
                height: 0;
            }

            /* The slider */
            .slider {
                --background: #28096b;
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: var(--background);
                transition: .5s;
                border-radius: 30px;
            }

            .slider:before {
                position: absolute;
                content: "";
                height: 1.4em;
                width: 1.4em;
                border-radius: 50%;
                left: 10%;
                bottom: 15%;
                box-shadow: inset 8px -4px 0px 0px #fff000;
                background: var(--background);
                transition: .5s;
            }

            input:checked + .slider {
                background-color: #522ba7;
            }

            input:checked + .slider:before {
                transform: translateX(100%);
                box-shadow: inset 15px -4px 0px 15px #fff000;
            }

            .login {
                transition-duration:0.5s;
                font-size: 17px;
                position: relative;
                display: inline-block;
                width: fit-content;
                height: 2em;
                padding:0.25em;
                background-color: #522ba7;
                cursor: pointer;
                border-radius: 30px;
            }

            .darkLogin {
                transition-duration:0.5s;
                font-size: 17px;
                position: relative;
                display: inline-block;
                width: fit-content;
                height: 2em;
                padding:0.25em;
                --background: #28096b;
                cursor: pointer;
                background-color: var(--background);
                border-radius: 30px;
            }

            .login:active {
                background-color: #522ba7;
            }

            #homeDivWelcome {
                height:94vh;
            }
        }
    </style>
</head>

<body onload="themeSetter()" class="font-body text-slate-900 bg-slate-100 dark:text-slate-100 overflow-x-hidden">

    <div class="min-h-screen flex flex-col w-screen">
        <div class="flex-1 w-screen backgroundBody1 dark:darkBackgroundBody1">
            <div class="w-screen backgroundBody2 dark:darkBackgroundBody2">

                <header class=" flex justify-between px-10">
                    <div class="flex items-center">
                        <h1><a href="{{ route('home') }}">PZAW</a></h1>
                    </div>

                    <div class="flex justify-center flex-wrap gap-20 pt-5 pb-5">
                        <div class="hover:underline"><a @class(['activeSite'=> request()->routeIs('home')]) href="{{ route('home') }}"><i class="bi bi-house"></i> Home</a></div>
                        <div class="hover:underline"><a @class(['activeSite'=> request()->routeIs('about_us')]) href="{{ route('about_us') }}"><i class="bi bi-info-circle"></i> About us</a></div>
                        <div class="hover:underline"><a @class(['activeSite'=> request()->routeIs('publications')]) href="{{ route('publications') }}"><i class="bi bi-book"></i> Publications</a></div>
                        <div class="hover:underline"><a @class(['activeSite'=> request()->routeIs('quotes')]) href="{{ route('quotes') }}"><i class="bi bi-chat-quote"></i> Quote List</a></div>
                    </div>

                    <div class="flex items-center gap-2">
                        <label class="switch">
                            <input type="checkbox" id="themeCheckbox" onclick="themeChanger()">
                            <span class="slider"></span>
                        </label>

                        @auth
                        <form action="{{ route('userLogout') }}" method="POST">
                            @csrf
                            @method('GET')
                                <input type="submit" class="h-fit text-white login dark:darkLogin" value="Logout">
                        </form>
                        @else
                            <button onclick=redirect("{{ route('userLoginForm') }}") class="h-fit text-white login dark:darkLogin">Login</button>
                        @endauth

                        <script>
                        function redirect(route) {
                            window.location.assign(route);
                        }
                        </script>
                    </div>
                </header>

                <div class="w-screen h-0">
                    <span class="spanGradient spanGradientX dark:darkSpanGradient"></span>
                </div>

                <contentOnGradient>
                    <x-alerts></x-alerts>
                    @yield('contentOnGradient')
                </contentOnGradient>
            </div>
        </div>

        <contentOffGradient>
            @yield('contentOffGradient')
        </contentOffGradient>

        <div>
            <footer class="bg-slate-800">
                <p class="text-center py-2 text-white">&copy 2023 Kacper Jaroszewski</p>
            </footer>
        </div>

    </div>

    <script>
    let themeCheckbox = document.getElementById("themeCheckbox");

    const d = new Date();
    d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function themeChanger() {
        themeCheckbox = document.getElementById("themeCheckbox");
        document.cookie = "theme=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

        if (themeCheckbox.checked == true) {
            document.cookie = "theme=light; " + expires + "; path=/";
            document.documentElement.classList.remove("dark");
        } else if (themeCheckbox.checked == false) {
            document.cookie = "theme=dark; " + expires + "; path=/";
            document.documentElement.classList.add("dark");
        }
    }

    function themeSetter() {
        let theme = getCookie("theme");
        if (theme == "") {
            document.cookie = "theme=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            document.cookie = "theme=dark; " + expires + "; path=/";
            themeCheckbox.checked = false;
            document.documentElement.classList.add("dark");
        } else {
            if (theme == "dark") {
                document.cookie = "theme=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                document.cookie = "theme=dark; " + expires + "; path=/";
                themeCheckbox.checked = false;
                document.documentElement.classList.add("dark");
            } else if (theme == "light") {
                document.cookie = "theme=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                document.cookie = "theme=light; " + expires + "; path=/";
                themeCheckbox.checked = true;
                document.documentElement.classList.remove("dark");
            }
        }
    }
    </script>

</body>

</html>