@extends('layouts/main')

@section('contentOnGradient')
    @foreach ($user as $usr)
        <div class="border-solid border-2 rounded-lg aboutUsDiv dark:darkAboutUsDiv p-10 mt-20 publicationsShowArticle">
            <h1 class="text-5xl">{{ $usr['name'] }}</h1>
            <p class="mt-2 text-xl text-gray-600 dark:text-gray-400">Data utworzenia konta: {{ $usr['created_at']->diffForHumans() }}</p>
            <p class="mt-5 text-lg"><a href="mailto:{{ $usr['email'] }}"><span class="text-gray-600 dark:text-gray-400">E-mail:</span> {{ $usr['email'] }}</a></p>
        </div>
        <h1 class="text-center purple mt-10">Publikacje użytkownika:</h1>
        <x-indexData :publicationData="$usr['publications']"></x-indexData>
    @endforeach

@endsection