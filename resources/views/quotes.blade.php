@extends('layouts/main')

@section('contentOnGradient')
    <h1 class="text-center purple mt-10">Lista postaci</h1>
    <div class="pb-20">
        <div class="pt-20 w-screen flex justify-center">
            <div class="flex flex-column p-10 justify-center gap-x-5 gap-y-2 flex-wrap darkPublicationsDivBackground rounded-3xl quoteCardDiv">
                @foreach($quotes as $quote)
                    <x-quote_card :quote="$quote" ></x-quote_card>
                @endforeach
            </div>
        </div>
    </div>
@endsection