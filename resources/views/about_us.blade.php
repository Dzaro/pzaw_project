@extends('layouts.main')

@section('contentOnGradient')
    <div class="pb-20">
        <div class="flex mx-40 border-solid border-2 rounded-lg aboutUsDiv dark:darkAboutUsDiv max-h-34 mt-20">
            <div class="w-3/6 m-10">
                <h1 class="purple">Kim jesteśmy?</h1>
                <p class="mt-10">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae sed optio inventore voluptate nesciunt tempora excepturi harum! Perspiciatis amet, aspernatur debitis illum facere porro autem, hic, sed dignissimos quas esse!</p>
                <p class="mt-10">Tempora excepturi ut provident? Voluptate ullam ea exercitationem non consectetur fugit ad atque, ex expedita vero dolores nostrum laudantium quaerat inventore pariatur. Dolorum inventore non, nisi ab, assumenda maxime aut sint exercitationem consequatur porro explicabo deleniti dicta natus rerum. Commodi, vero! Similique, temporibus delectus?</p>
                <p class="mt-10">Voluptatum soluta, quis mollitia iusto optio ad facilis quaerat ullam nemo debitis nam suscipit, voluptate at rem ea amet perferendis? Autem, voluptate.</p>
            </div>
            <span class="w-0.5 my-10 spanGradient dark:darkSpanGradient"></span>
            <div class="w-3/6 m-10">
                <h1 class="purple">Kontakt</h1>
                <p class="mt-10">Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis fuga similique amet impedit et officia earum optio distinctio dicta nemo consequuntur nam sunt ea, minus cumque obcaecati saepe omnis itaque.</p>
                <p class="mt-10" ><a href="mailto:examplemail@gmail.com">Gmail: examplemail@gmail.com</a></p>
                <p class="mt-5"><a href="tel:48839194472">Telefon: +48 839 194 472</a></p>
                <p class="mt-5">Facebook: PZAW PROJECT</p>
                <p class="mt-5">Siedziba firmy: Warszawa, Żoliborz, Słowackiego 12/7, 3 piętro</p>
            </div>
        </div>
    </div>
@endsection