@extends('layouts/main')

@section('contentOnGradient')
<h1 class="text-center purple mt-10">wszystkie publikacje:</h1>
@auth
    <p class="w-full flex justify-center"><a class="underline purple" href="{{ route('publicationCreate') }}">Stwórz publikację</a></p>
@endauth
<x-indexData :publicationData="$publicationData"></x-indexData>
@endsection