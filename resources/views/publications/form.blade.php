@extends('layouts/main')

@php
    $action = route('publicationStore');
    $title = null;
    $content = null;
    $author_id = null;

    if(! empty($publication)) {
        $title = $publication->title;
        $content = $publication->content;
        $author_id = $publication->author_id;
        $action = route('publicationUpdate', ['publication' => $publication]);
    }
@endphp


@section('contentOnGradient')

    @if (! empty($publication))
        <h1 class="text-center purple mt-10">Edytowanie Publikacji</h1>
    @else
        <h1 class="text-center purple mt-10">Dodawanie Publikacji</h1>
    @endif

    <div class="pb-20">
        <div class="mx-40 mt-20">
            <form action="{{ $action }}" method="POST">
                @if (! empty($publication))
                    @method('PUT')
                @endif

                <div class="flex flex-col items-center">

                    @csrf
                    <input type="hidden" name="author_id" value="{{ Auth::user()->id }}">
                    <div class="w-full mb-10">
                        <input placeholder="title" type="text" name='title' value="{{ $title }}" class="text-xl appearance-none w-full min-w-[500px] border-solid border-2 p-2 aboutUsDiv darkPublicationsArticleBackground dark:darkAboutUsDiv">
                        @error('title')
	                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class=" w-full mb-10">
                    <textarea placeholder="content" type="text" name='content' class="text-xl appearance-none w-full min-w-[500px] border-solid border-2 p-2 aboutUsDiv darkPublicationsArticleBackground dark:darkAboutUsDiv h-60">{{ $content }}</textarea>
                    @error('content')
	                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                    @enderror
                    </div>

                    <div class="w-full flex justify-between">
                        <div class="">
                            @error('author_id')
	                            <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>

                        <input type="submit">
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection