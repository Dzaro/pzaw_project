@extends('layouts/main')

@section('contentOnGradient')
    <x-publicationData :publicationData="$publication" :comments="$comments"></x-publicationData>
    <h1 class="text-center purple mt-20">POZOSTAŁE ARTYKUŁY</h1>

    <x-indexData :publicationData="$publicationData"></x-indexData>
</div>
@endsection