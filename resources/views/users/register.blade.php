@extends('layouts/main')

@php
    $action = route('userStore');
    $name = null;
    $email = null;
    $password = null;
    $password_confirmation = null;
@endphp


@section('contentOnGradient')
<h1 class="text-center purple mt-10">Zarejestruj się</h1>

<div class="pb-20">
    <div class="mx-40 mt-20">

        <form action="{{ $action }}" method="POST">

            <div class="flex flex-col items-center">
                <div class="border-solid border-2 p-10 w-[500px] min-w-[300px] aboutUsDiv dark:darkAboutUsDiv">
                    <p class="text-xl pb-5 text-center">Dołącz do <span class="purple">najszybszego źródła</span> informacji o naszej szkole już dziś!</p>

                    <div class="flex flex-col items-center">

                        @csrf

                        <div class="w-full mb-5">
                            <input type="text" name="name" placeholder="Imię i nazwisko" class="apperance-none bg-transparent border-none w-full outline-none focus:placeholder:purple focus:purple focus:[border-color:rgb(152,106,220)] [border-bottom:solid] [border-color:black] [border-width:2px] dark:[border-color:white] dark:focus:[border-color:rgb(152,106,220)]">
                            @error('name')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-full mb-5">
                            <input type="text" name="email" placeholder="E-mail" class="apperance-none bg-transparent border-none w-full outline-none focus:placeholder:purple focus:purple focus:[border-color:rgb(152,106,220)] [border-bottom:solid] [border-color:black] [border-width:2px] dark:[border-color:white] dark:focus:[border-color:rgb(152,106,220)]">
                            @error('email')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-full mb-5">
                            <input type="password" name="password" placeholder="Hasło" class="apperance-none bg-transparent border-none w-full outline-none focus:placeholder:purple focus:purple focus:[border-color:rgb(152,106,220)] [border-bottom:solid] [border-color:black] [border-width:2px] dark:[border-color:white] dark:focus:[border-color:rgb(152,106,220)]">
                            @error('password')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-full mb-10">
                            <input type="password" name="password_confirmation" placeholder="Powtórz hasło" class="apperance-none bg-transparent border-none w-full outline-none focus:placeholder:purple focus:purple focus:[border-color:rgb(152,106,220)] [border-bottom:solid] [border-color:black] [border-width:2px] dark:[border-color:white] dark:focus:[border-color:rgb(152,106,220)]">
                            @error('password')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>
                        <input class="purple hover:cursor-pointer hover:underline mb-5" type="submit" value="Zarejestruj się">
                        <a class="purple hover:cursor-pointer hover:underline" href="{{ route('userLogin') }}">Masz już konto? Zaloguj się</a>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection