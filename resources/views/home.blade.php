@extends('layouts/main')

@section('contentOnGradient')
    <div class="w-screen flex items-center justify-center" id="homeDivWelcome" >
    <script>
        function scrollDown() {
            let Element = document.getElementById("scrollToElement");
            let xOffset = Element.offsetTop;
            window.scrollTo({
                top: xOffset,
                left: 0,
                behavior: "smooth",
            });
        }
    </script>
        <p onclick="scrollDown()" class="absolute text-7xl darkHomeArrowAnimation dark:homeArrowAnimation"><i class="bi bi-arrow-down"></i></p>
        <p class="leading-normal text-center font-black text-7xl">Twoja Podróż po<br>SCI zaczyna <br>się <span class="darkpurple dark:purple">tutaj.</span></p>
    </div>
@endsection

@section('contentOffGradient')
<div class="w-screen h-screen pb-32 homeContent dark:darkHomeContent flex justify-center flex-col">

        <div>
            <h1 id="scrollToElement" class="text-center darkpurple dark:purple">Dzisiaj jest</h1>
            <p class="text-6xl text-center">{{ $date }}</p>
        </div>

        <div class="mt-32">
            <div class="mx-10">

                <h1 class="text-center darkpurple dark:purple">Najnowszy artykuł:</h1>
                @foreach($lastPublication as $publication)
                <div class="mx-40 border-solid border-2 rounded-lg aboutUsDiv dark:darkAboutUsDiv p-10 mt-5">
                    <p class="text-5xl"><b>{{ $publication['title'] }}</b></p>
                    <p class="mt-2 text-xl text-gray-600 dark:text-gray-400"><a href="{{ route('userShow', ['id' => $publication->author_id]) }}"> {{ $publication['author']['name'] }}</a>, {{ $publication['created_at']->diffForHumans() }}</p>
                    <p class="mt-5 text-lg">{{ $publication['excerpt'] }} <a class="underline darkpurple dark:purple" href="{{ route('publicationShow', ['id' => $publication->id]) }}">Czytaj więcej</a></p>
                </div>
                @endforeach

            </div>
        </div>

        <div class="mt-5 text-center hover:underline"><a class="" href="{{ route('publications') }}">Przejdź do wszystkich artykułów <i class="bi bi-arrow-right-short"></i></a></div>

</div>
@endsection