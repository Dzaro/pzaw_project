<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Publication;
use App\Models\User;
use App\Models\Comment;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        // \App\Models\Publication::factory(10)->create();
        $reporter1 = User::factory()->create([
            'name' => 'Henrykos Reporteros',
            'email' => 'henrykosreporter@gmail.com',
        ]);

        $reporter2 = User::factory()->create([
            'name' => 'Henryk Ciekawski',
            'email' => 'henrykciekawski98@gmail.com',
        ]);

        $reporter3 = User::factory()->create([
            'name' => 'Henryk Bedosiar',
            'email' => 'henrykbedosiar03@gmail.com',
        ]);

        $reporter4 = User::factory()->create([
            'name' => 'Henryk III Nieśmiertelny',
            'email' => 'henrykniesmiertelnytrzeci@gmail.com',
        ]);

        $reporter5 = User::factory()->create([
            'name' => 'Test testowski',
            'email' => 'test@gmail.com',
            'password' => 'test',
        ]);

        $publication1 = Publication::create([
            'title' => 'Samoloty uczniów SCI użyte do ataku terrorystycznego w Szczecinie',
            'content' => 'Uczniowie klas trzecich od początku listopada odbywali warsztaty organizowane przez firmę Codelab o tematyce zarządzania pracą w grupie. Częścią praktyczną zajęć było skonstruowanie samolotów, które w założeniu miały służyć nauce praktycznej o pracy wspólnej. Dzisiaj rano jednak Szczecin obiegły koszmarne wieści, a raczej koszmarny huk. Mieszkańcy opisują to jako coś strasznego "Ściany się zatrzęsły, cały blok wydawał się odrywać od ziemi. Towarzyszył temu ogromny hałas, z każdą sekundą nabierający na sile. Nie wiedziałam co robić, co się dzieje". Jak się okazało, powodem całego zamieszania były samoloty uczniów SCI, które wleciały w Hanza Tower, najwyższy wieżowiec w mieście, następnie powodując jego zawalenie. Służby całego miasta i okolicznych gmin zostały postawione w stan gotowości. Miasto jeszcze nie skomentowało tragedii, lecz zakłada się liczne osoby martwe i ciężko ranne.',
            'author_id' => 3,
            'created_at' => '2023-11-13 18:20:17',
            'updated_at' => '2023-11-13 18:20:17',
        ]);

        $publication2 = Publication::create([
            'title' => 'Turniej piłki nożnej w SCI sabotowany',
            'content' => 'Turniej piłki nożnej odbywający się dnia 21 października o godzinie 9:00 został sabotowany przez własnych uczestników. Stwierdzili oni, że nie zgadzają się na uczestnictwo drużyny nauczycieli w turnieju, a tym samym jednogłośnie odmówili dalszych rozgrywek. Sporu nie udało się załagodzić, a turniej został odwołany.',
            'author_id' => 4,
            'created_at' => '2023-10-20 17:27:42',
            'updated_at' => '2023-10-20 17:27:42',
        ]);

        $publication3 = Publication::create([
            'title' => 'Admin SCI napadnięty, wszystkie dane uczniów z moodle\'a zagrożone publicznym ujawnieniem',
            'content' => 'Admin SCI został wczoraj napadnięty przez zgraję pierwszaków. Jak mówi ofiara, wyszedł ze szkoły i poszedł w lewo, gdzie zza rogu wyłoniła się grupa zamaskowanych 5 pierwszaków, którzy grozili mu "odcięciem palca", jeśli nie da danych logowania do konta administratora na moodle\'u. Tym samym nieznani pierwszacy weszli w posiadanie intymnych danych uczniów zebranych w ankiecie Pani pedagog.',
            'author_id' => 1,
            'created_at' => '2023-10-20 18:18:18',
            'updated_at' => '2023-10-20 18:18:18',
        ]);

        $publication4 = Publication::create([
            'title' => 'Nieznany żartowniś rozwiesił w całej szkole zdjęcia mysz',
            'content' => 'Uczniowie byli dzisiaj świadkiami niecodziennego widoku, bowiem na klatkach schodowych i auli mogli spotkać liczne kartki A4 z nadrukowanymi myszami. Uczniowie podeszli do ciekawego zdarzenia z dystansem, jednak władze szkoły zapowiedziały znalezienie winowajcy i ukaranie go. Jak ich przedstawiciel, Pan Kopałko, mówi, "shhsshhsh". Redaktor przekazał nam, że mówił ciszej od wiatraka, dlatego też niezbyt wiele można wyciągnąć z cytowanej powyżej wypowiedzi.',
            'author_id' => 1,
            'created_at' => '2023-10-20 18:18:30',
            'updated_at' => '2023-10-20 18:18:30',
        ]);

        $publication5 = Publication::create([
            'title' => 'Turniej ping-ponga odwołany z powodu niedostatecznej ilości stołów',
            'content' => 'Ku niezadowoleniu społeczności szkolnej, turniej ping-ponga został oficjalnie odwołany z powodu braku stołów, które rok temu zostały bezdusznie zniszczone przez pierwszaki. Władze szkoły komentują kontrowersje w następujacy sposób: "Jak chcecie to grajcie na zarobaczonej i nie mytej podłodze na auli, pieniądze samorządu nie są na stoły od ping-ponga."',
            'author_id' => 2,
            'created_at' => '2023-10-20 20:13:32',
            'updated_at' => '2023-10-20 20:13:32',
        ]);

        $publication6 = Publication::create([
            'title' => 'Nauczyciele wywołali bunt w szkole. "Nie chcemy więcej podwójnych lekcji"',
            'content' => 'Od dawna w naszej szkole praktykowane są zastępstwa dla nauczycieli, którzy w tym samym czasie mają lekcje z inną klasą, tzw. "podwójne zastępstwa". Nauczyciele powiedzieli stop temu stanu rzeczy i jednogłośnie stwierdzili, że czas zakończyć ten skandal. Władze szkoły wstrzymują się od komentarza w tej sprawie.',
            'author_id' => 3,
            'created_at' => '2023-10-20 20:13:32',
            'updated_at' => '2023-10-20 20:13:32',
        ]);

        $publication7 = Publication::create([
            'title' => 'Większością samorządową uchwalono zmiany w regulaminie zarządzania pieniędzmi samorządu',
            'content' => 'Samorząd wprowadził nowe wytyczne dotyczące wydawania zebranych na początku roku pieniędzy. Od teraz, każdy wydatek powyżej 1000zł będzie musiał zostać skonsultowany nie tylko z władzami szkoły, lecz również z gospodarzami klas. Krążą plotki, że władze szkoły coraz mniej są zadowolone z utraty władzy absolutnej, na co mają zamiar wkrótce odpowiedzieć.',
            'author_id' => 4,
            'created_at' => '2023-10-20 20:13:33',
            'updated_at' => '2023-10-20 20:13:33',
        ]);

        $publication8 = Publication::create([
            'title' => 'Ta publikacja należy do użytkownika test@gmail.com',
            'content' => 'logując się na konto test@gmail.com użytkownik jest w stanie edytować oraz usunąć tą publikację',
            'author_id' => 5,
            'created_at' => '2024-01-20 20:13:33',
            'updated_at' => '2024-01-20 20:13:33',
        ]);
        
        $comment1 = Comment::create([
            'author_id' => $reporter3->id,
            'publication_id' => $publication7->id,
            'content' => 'komentarz1',
            'created_at' => '2023-11-25 17:01:39',
            'updated_at' => '2023-11-25 17:01:39',
        ]);

        $comment2 = Comment::create([
            'author_id' => $reporter2->id,
            'publication_id' => $publication7->id,
            'content' => 'komentarz2',
            'created_at' => '2023-11-25 19:37:20',
            'updated_at' => '2023-11-25 19:37:20',
        ]);

        $comment3 = Comment::create([
            'author_id' => $reporter3->id,
            'publication_id' => $publication2->id,
            'content' => 'komentarz3',
            'created_at' => '2023-11-25 23:01:20',
            'updated_at' => '2023-11-25 23:01:20',
        ]);

        $comment4 = Comment::create([
            'author_id' => $reporter1->id,
            'publication_id' => $publication7->id,
            'content' => 'komentarz4',
            'created_at' => '2023-11-25 20:42:51',
            'updated_at' => '2023-11-25 20:42:51',
        ]);

        $comment5 = Comment::create([
            'author_id' => $reporter2->id,
            'publication_id' => $publication7->id,
            'content' => 'odpowiadam na komentarz o id1',
            'parent_id' => 1,
            'created_at' => '2023-11-25 21:20:32',
            'updated_at' => '2023-11-25 21:20:32',
        ]);

        $comment6 = Comment::create([
            'author_id' => $reporter3->id,
            'publication_id' => $publication7->id,
            'content' => 'odpowiadam na komentarz o id1',
            'parent_id' => 1,
            'created_at' => '2023-11-25 21:43:48',
            'updated_at' => '2023-11-25 21:43:48',
        ]);

        $comment7 = Comment::create([
            'author_id' => $reporter1->id,
            'publication_id' => $publication7->id,
            'content' => 'odpowiadam na komentarz o id5',
            'parent_id' => 5,
            'created_at' => '2023-11-25 20:42:51',
            'updated_at' => '2023-11-25 20:42:51',
        ]);

        $comment8 = Comment::create([
            'author_id' => $reporter4->id,
            'publication_id' => $publication4->id,
            'content' => 'komentarz5',
            'created_at' => '2023-11-25 23:30:15',
            'updated_at' => '2023-11-25 23:30:15',
        ]);

        $comment9 = Comment::create([
            'author_id' => $reporter3->id,
            'publication_id' => $publication7->id,
            'content' => 'komentarz6',
            'created_at' => '2023-11-25 22:30:15',
            'updated_at' => '2023-11-25 22:30:15',
            'deleted_at' => '2023-11-25 22:35:36',
        ]);

    }
}
