<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();

            $table->foreignId('author_id')
	        ->references('id')
	        ->on('users');
            
            $table->foreignId('publication_id')
            ->references('id')
            ->on('publications');

            $table->string('content', 500);

            $table->foreignId('parent_id')
            ->nullable()
            ->references('id')
	        ->on('comments')
            ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
};
