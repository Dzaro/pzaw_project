<?php

namespace App\View\Components;

use Illuminate\View\Component;

class publicationData extends Component
{
    public $publicationData;

    public function __construct($publicationData)
    {
        $this->publicationData = $publicationData;
    }

    public function render()
    {
        return view('components.publicationData');
    }
}