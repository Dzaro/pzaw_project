<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
            'remember_me' => ['boolean'],
        ];
    }

    public function messages(): array
    {
        return [
            'email.required' => 'E-mail jest wymagany',
            'email.email' => 'Podany ciąg znaków nie jest E-mailem',
            'password.required' => 'Hasło jest wymagane',
            'remember_me.boolean' => 'Wystąpił problem podczas zczytywania pola "Zapamiętaj mnie". Spróbuj ponownie',
        ];
    }
}
