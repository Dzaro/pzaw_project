<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'between:3,50'],
            'content' => ['required', 'string', 'between:10,500'],
            'author_id' => ['required', 'integer', 'exists:users,id'],
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => 'Tytuł jest wymagany',
            'title.string' => 'Tytuł musi być ciągiem znaków',
            'title.between' => 'Długość tytułu musi zawierać się w zakresie 3-50 znaków',
            'content.required' => 'Treść jest wymagana',
            'content.string' => 'Treść musi być ciągiem znaków',
            'content.between' => 'Długość treści musi zawierać się w zakresie 10-500 znaków',
            'author_id.required' => 'Id autora jest wymagane',
            'author_id.integer' => 'Id autora musi być liczbą',
            'author_id.exists' => 'Autor o takim Id nie istnieje',
        ];
    }
}
