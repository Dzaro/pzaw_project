<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'author_id' =>'required',
            'publication_id' =>'required',
            'parent_id' =>'nullable',
            'content' =>['required', 'min:3', 'max:300'],
        ];
    }

    public function messages(): array {
        return [
            'author_id.required' => 'Wystąpił błąd podczas zczytywania danych użytkownika. Spróbuj jeszcze raz albo zaloguj się ponownie.',
            'publication_id.required' => 'Wystąpił błąd podczas zczytywania danych publikacji. Spróbuj jeszcze raz.',
            'parent_id.nullable' => 'Wystąpił błąd podczas zczytywania danych komentarza, na który odpowiadasz. Spróbuj jeszcze raz.',
            'content.required' => 'Treść jest wymagana.',
            'content.min' => 'Komentarz musi mieć minimum 3 znaki.',
            'content.max' => 'Komentarz może mieć maksymalnie 100 znaków.',
        ];
    }
}
