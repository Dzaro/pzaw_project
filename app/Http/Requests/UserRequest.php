<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:50'],
            'email' => ['required', 'string', 'unique:users,email', 'email'],
            'password' => ['required', 'string', 'confirmed', Password::defaults()],
        ];
    }
    public function messages(): array
    {
        return [
            'name.required' => 'Imię i nazwisko są wymagane',
            'name.string' => 'Imię i nazwisko muszą być ciągiem znaków',
            'name.min' => 'Imię i nazwisko jest za krótkie',
            'name.max' => 'Imię i nazwisko jest za długie',
            'email.required' => 'E-mail jest wymagany',
            'email.unique' => 'Konto na podany e-mail już istnieje',
            'email.email' => 'Podany ciąg znaków nie jest E-mailem',
            'password.required' => 'Hasło jest wymagane',
            'password.confirmed' => 'Hasła nie są takie same',
            'password.min' => 'Hasło wymaga minimum 6 znaków',
            #TODO dodac pozostale messages dla wszystkich rules z Password::defaults()
        ];
    }
}
