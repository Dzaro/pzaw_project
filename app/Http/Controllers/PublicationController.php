<?php

namespace App\Http\Controllers;

use App\Models\Publication;
use App\Models\User;
use App\Models\Comment;
use App\Http\Requests\PublicationRequest;
use App\Http\Requests\UpdatePublicationRequest;
use Illuminate\Http\Request;

class PublicationController extends Controller
{

    public function index() {
        $publicationData = Publication::orderBy('created_at', 'desc')->get();

        return view('publications/index', ['publicationData' => $publicationData]);
    }


    public function show(int $id) {
        $publicationData = Publication::orderBy('created_at', 'desc')->get();
        $publication = [0 => Publication::where('id', $id)->firstOrFail()];
        $comments = Comment::withTrashed()->orderBy('parent_id', 'asc')->orderBy('id', 'asc')->where('publication_id', $id)->get();

        return view(
            'publications/show',
            [
                'publication' => $publication,
                'comments' => $comments,
                'publicationData' => $publicationData,
            ]
        );
    }


    public function create() {
        $users = User::all();
        
        return view(
            'publications/form',
            ['users' => $users]
        );
    }


    public function store(PublicationRequest $request) {
        $data = $request->validated();
        //dd($data);
        $newPublication = new Publication($data);
        
        $newPublication->save();

        return redirect()->route('publicationShow', $newPublication)->with('success', 'Akcja pomyślnie wykonana');
    }


    public function edit(int $id) {
        $publication = Publication::where('id', $id)->firstOrFail();
        $this->authorize('update', $publication);

        $users = User::all();

        return view(
            'publications/form',
            [
                'users' => $users,
                'publication' => $publication,
            ]
        );
    }


    public function update(UpdatePublicationRequest $request, Publication $publication) {
        $this->authorize('update', $publication);

        $data = $request->validated();
        $publication->fill($data);
        
        $publication->save();

        return redirect()->route('publicationShow', $publication)->with('success', 'Akcja pomyślnie wykonana');
    }


    public function delete(Publication $publication) {
        $this->authorize('destroy', $publication);
        $publication->comments()->forceDelete();
        $publication->delete();

        return redirect()->route('publications')->with('success', 'Publikacja została usunięta');
    }

}