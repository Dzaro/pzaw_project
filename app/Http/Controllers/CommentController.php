<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Models\Comment;

class CommentController extends Controller
{
    
    public function store(CommentRequest $request) {
        $data = $request->validated();
        
        $comment = new Comment($data);
        //dd($comment);
        $comment->save();

        return redirect()->route('publicationShow', $comment->publication_id)->with('success', 'Komentarz pomyślnie dodany.');
    }

    public function destroy(Comment $comment) {
        $comment->delete();

        return redirect()->route('publicationShow', $comment->publication_id)->with('success', 'Komentarz pomyślnie usunięty.');
    }
}
