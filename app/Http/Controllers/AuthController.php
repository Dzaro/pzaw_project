<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    function form() {
        return view('users/login');
    }

    function login(LoginRequest $request) {

        $credentials = [
            'email' => $request->validated('email'),
            'password' => $request->validated('password'),
        ];

        if(Auth::attempt($credentials, $request->boolean('remember_me'))) {
            $request->session()->regenerate();
            return redirect()->route('publications')->with('success', 'Pomyślnie zalogowano');;
        }
        else {
            return back()->withErrors([
                'login' => 'Login lub hasło są niepoprawne.'
            ]);
        }
    }

    function logout(Request $request) {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('publications')->with('success', 'Pomyślnie wylogowano');
    }
}
