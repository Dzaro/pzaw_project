<?php

namespace App\Http\Controllers;

use App\Models\Publication;
use App\Models\User;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function home() {
        return view('home', ['date' => now(), 'lastPublication' => Publication::orderBy('created_at', 'desc')->limit(1)->get()]);
    }

    public function aboutUs() {
        return view('about_us');
    }

    public function quotes() {
        $quotes = [
            1 => [
                'quote' => 'You were a boulder... I am a mountain.',
                'hero' => 'Sage',
                'role' => 'Sentinel',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/7/74/Sage_icon.png',
            ],
            2 => [
                'quote' => 'Racing to the spike side!',
                'hero' => 'Jett',
                'role' => 'Duelist',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/3/35/Jett_icon.png',
            ],
            3 => [
                'quote' => 'Call me tech support again.',
                'hero' => 'Killjoy',
                'role' => 'Sentinel',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/1/15/Killjoy_icon.png',
            ],
            4 => [
                'quote' => 'One of my cameras is broken!... oh wait... it\'s fine.',
                'hero' => 'Cypher',
                'role' => 'Sentinel',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/8/88/Cypher_icon.png',
            ],
            5 => [
                'quote' => 'I am the beggining. I am the end.',
                'hero' => 'Omen',
                'role' => 'Controller',
                'image' => 'https://static.wikia.nocookie.net/valorant/images/b/b0/Omen_icon.png',
            ],
        ];

        return view('quotes', ['quotes' => $quotes]);
    }

    public function userShow(int $id) {
        $temp = [1 => User::where('id', $id)->firstOrFail()];

        return view('userShow', ['user' => $temp]);
    }
}
