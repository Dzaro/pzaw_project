<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register() {
        return view('users/register');
    }

    public function store(UserRequest $request) {
        $data = $request->validated();
        //dd($data);

        $newUser = new User($data);
        
        $newUser->save();
        
        return redirect()->route('userLoginForm')->with('success', 'Pomyślnie stworzono konto. Zaloguj się!');
    }
}
