<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
* @property int $id
* @property string $title
* @property string $content
* @property int $author_id
*
* @property-read User $author
* @property-read Comment $comments
*/

class Publication extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'creationDate',
        'author_id',
    ];

    protected function excerpt(): Attribute
    {
        return Attribute::make(
            get: fn($value, $attributes) => substr($attributes['content'], 0, 100)."...",
        );
    }

    public function author(): BelongsTo
    {
	    return $this->belongsTo(User::class, 'author_id')->withTrashed();
    }

    public function comments(): HasMany
    {
	    return $this->hasMany(Comment::class, 'publication_id');
    }

}
