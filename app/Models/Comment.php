<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* @property int $id
* @property int $author_id
* @property int $publication_id
* @property string $content
*
*/

class Comment extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'author_id',
        'publication_id',
        'parent_id',
        'content',
    ];

    public function author(): BelongsTo
    {
	    return $this->belongsTo(User::class, 'author_id')->withTrashed();
    }

    public function child_comments(): HasMany
    {
	    return $this->hasMany(Comment::class, 'parent_id');
    }
}
