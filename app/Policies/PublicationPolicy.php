<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Publication;

class PublicationPolicy
{
    /**
     * Create a new policy instance.
     */
    public function __construct()
    {
        //
    }

    public function update(?User $user, Publication $publication): bool
    {
        if(isset($user) && $user->id == $publication->author_id) {
                return true;
        }
        else {
            return false;
        }
    	return false;
    }

    public function destroy(?User $user, Publication $publication): bool
    {
        if(isset($user) && $user->id == $publication->author_id) {
            return true;
        }
        else {
            return false;
        }
        return false;
    }
}
